var cardsListC6621BD74E6948688D7CFA5D1BA3C3E1_DEBUG;
/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ 265:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   u: () => (/* binding */ Visual)
/* harmony export */ });
/*
*  Power BI Visual CLI
*
*  Copyright (c) Microsoft Corporation
*  All rights reserved.
*  MIT License
*
*  Permission is hereby granted, free of charge, to any person obtaining a copy
*  of this software and associated documentation files (the ""Software""), to deal
*  in the Software without restriction, including without limitation the rights
*  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
*  copies of the Software, and to permit persons to whom the Software is
*  furnished to do so, subject to the following conditions:
*
*  The above copyright notice and this permission notice shall be included in
*  all copies or substantial portions of the Software.
*
*  THE SOFTWARE IS PROVIDED *AS IS*, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
*  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
*  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
*  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
*  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
*  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
*  THE SOFTWARE.
*/


class Visual {
    target;
    host;
    categories;
    listOfCardVisual;
    cardVisualList;
    constructor(options) {
        // constructor body
        this.target = options.element;
        this.target.style.overflow = 'auto'; // add scroll bar
        this.host = options.host;
        // this.categories = document.createElement("pre");
        // this.target.appendChild(this.categories);
        this.listOfCardVisual = document.createElement("div");
        this.listOfCardVisual.setAttribute("class", "list");
        this.target.appendChild(this.listOfCardVisual);
        //this.listOfCardVisual.className
        // ...
    }
    update(options) {
        const dataView = options.dataViews[0];
        const categoricalDataView = dataView.categorical;
        if (!categoricalDataView ||
            !categoricalDataView.categories ||
            !categoricalDataView.categories[0] ||
            !categoricalDataView.values) {
            return;
        }
        // console.log(dataView.categorical.categories[0].values);
        const valuesData = dataView.categorical.values.grouped();
        const label = valuesData[0].values[0].source.displayName;
        const values = valuesData[0].values[0].values;
        const categories = dataView.categorical.categories[0].values;
        // console.log(valuesData);
        // console.log(label);
        // console.log(values);
        console.log(valuesData[0].values);
        for (let i = 0; i < values.length; i++) {
            this.createCardElement(categories[i], label, values[i]);
        }
    }
    createCardElement(category, label, value) {
        const titleElement = document.createElement("h3");
        titleElement.textContent = category;
        const labelElement = document.createElement("h6");
        labelElement.textContent = label;
        const valueElement = document.createElement("p");
        valueElement.textContent = value.toString();
        const cardDiv = document.createElement("div");
        cardDiv.setAttribute("class", "card");
        cardDiv.appendChild(titleElement);
        cardDiv.appendChild(labelElement);
        cardDiv.appendChild(valueElement);
        this.listOfCardVisual.appendChild(cardDiv);
    }
}


/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be isolated against other modules in the chunk.
(() => {
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _src_visual__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(265);

var powerbiKey = "powerbi";
var powerbi = window[powerbiKey];
var cardsListC6621BD74E6948688D7CFA5D1BA3C3E1_DEBUG = {
    name: 'cardsListC6621BD74E6948688D7CFA5D1BA3C3E1_DEBUG',
    displayName: 'cardsList',
    class: 'Visual',
    apiVersion: '5.3.0',
    create: (options) => {
        if (_src_visual__WEBPACK_IMPORTED_MODULE_0__/* .Visual */ .u) {
            return new _src_visual__WEBPACK_IMPORTED_MODULE_0__/* .Visual */ .u(options);
        }
        throw 'Visual instance not found';
    },
    createModalDialog: (dialogId, options, initialState) => {
        const dialogRegistry = globalThis.dialogRegistry;
        if (dialogId in dialogRegistry) {
            new dialogRegistry[dialogId](options, initialState);
        }
    },
    custom: true
};
if (typeof powerbi !== "undefined") {
    powerbi.visuals = powerbi.visuals || {};
    powerbi.visuals.plugins = powerbi.visuals.plugins || {};
    powerbi.visuals.plugins["cardsListC6621BD74E6948688D7CFA5D1BA3C3E1_DEBUG"] = cardsListC6621BD74E6948688D7CFA5D1BA3C3E1_DEBUG;
}
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (cardsListC6621BD74E6948688D7CFA5D1BA3C3E1_DEBUG);

})();

cardsListC6621BD74E6948688D7CFA5D1BA3C3E1_DEBUG = __webpack_exports__;
/******/ })()
;
//# sourceMappingURL=https://localhost:8080/assets/visual.js.map